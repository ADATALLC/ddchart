module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  rules: {
    'no-console': 'off',
    'no-debugger': 'off',
    'vue/attribute-hyphenation': [
      'error',
      'always'
    ],
    'vue/html-indent': [
      'error',
      2
    ],
    'vue/attributes-order': 'error',
    'vue/html-quotes': [
      'error',
      'single'
    ]
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}