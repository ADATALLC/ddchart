import Vuex from 'vuex'
import { state, mutations } from './../../src/store'
import { mutation } from './../../src/enums/vuex-storage.mutations'
import ChartEntry from './../../src/classes/ChartEntry'
import { expect } from 'chai'

let $store = new Vuex.Store({
  state,
  mutations
})

let entries = Object.freeze([
  new ChartEntry({
    id: 1,
    date: new Date(),
    value: 10
  }),
  new ChartEntry({
    id: 2,
    date: new Date(),
    value: 2
  }),
  new ChartEntry({
    id: 3,
    date: new Date(),
    value: -3
  })
])

describe('vuex spec', () => {
  it('store is empty when created', () => {
    expect($store.state.entries).to.deep.equal([])
  })
  it('add one entry', () => {
    $store.commit(mutation.M_ADD_ENTRY, entries[0])
    expect($store.state.entries.length).equal(1)
  })
  it('can not add entry with the same \'id\'', () => {
    $store.commit(mutation.M_ADD_ENTRY, entries[0])
    expect($store.state.entries.length).equal(1)
  })
  it('add two more entries (3 total now)', () => {
    $store.commit(mutation.M_ADD_ENTRY, entries[1])
    $store.commit(mutation.M_ADD_ENTRY, entries[2])
    expect($store.state.entries.length).equal(3)
  })
  it('remove entry with \'id\'', () => {
    let id = entries[1].id
    $store.commit(mutation.M_REMOVE_ENTRY, id)
    expect($store.state.entries.map(e => e.id)).to.not.include(id)
  })
  it('should add only removed entry', () => {
    for (let entry of entries) {
      $store.commit(mutation.M_ADD_ENTRY, entry)
    }
    expect($store.state.entries.map(e => e.id)).to.include.members(entries.map(e => e.id))
  })
  it('should remove all entries', () => {
    $store.commit(mutation.M_REMOVE_ALL)
    expect($store.state.entries).to.deep.equal([])
  })
})
