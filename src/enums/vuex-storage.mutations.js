/*
*
* Типы 'мутаций' Vuex
*
*/

export const mutation = Object.freeze({
  M_ADD_ENTRY: 'addEntry',
  M_INSERT_ENTRY: 'insertEntry',
  M_UPDATE_ENTRY: 'updateEntry',
  M_REMOVE_ENTRY: 'removeEntry',
  M_REMOVE_ALL: 'removeAll'
})
