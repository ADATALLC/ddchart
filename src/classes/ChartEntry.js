import md5 from 'md5'

/**
*
* Структура для хранения данных диаграммы
*
*/
export default function ChartEntry (data = {}) {
  this.id = data.id || md5(new Date())
  this.value = data.value || 0

  let date = new Date(data.date)
  this.date = isNaN(date) ? new Date() : date
}

ChartEntry.prototype = {
  constructor: ChartEntry,

  getDate: function () {
    return `${this.date.getMinutes()}:${this.date.getSeconds()}:${this.date.getMilliseconds()}`
  },

  getValue: function () {
    return this.value
  }
}
