import * as d3 from 'd3'
import ChartEntry from './ChartEntry'

/**
 * Класс, выполняющий отрисовку графика в заданной области элемента
 *
 * @export
 * @class Graph
 */
export default class Graph {
  /**
   * @param {HTMLElement} [root=null] Ссылка на элемент, в который внедряемся
   * @param {Object{x,y}} [inset=null] Отступы от границ root по Х и Y
   * @memberof Graph
   */
  constructor (root = null, inset = null) {
    this.root = root
    this.inset = inset || {
      x: 40,
      y: 20
    }
  }
}

Graph.prototype = {
  constructor: Graph,

  draw: function (entries) {
    if (this.root === null) {
      return console.error('Graph: can not draw without parent element')
    }

    let svgSize = this.root.getBoundingClientRect()
    let svg = this.resizeSVG(this.getSVG(), svgSize)

    this.clearSVG(svg)

    let canvas = this.buildCanvas(svg)
    let canvasSize = this.getCanvasSize(svgSize)

    if (entries.length <= 0 || !entries.every(e => e instanceof ChartEntry)) {
      this.noDataToDraw(canvas, canvasSize)
    } else {
      this.drawGraph(canvas, canvasSize, entries)
    }
  },

  noDataToDraw: function (canvas, canvasSize) {
    canvas.append('text')
      .text('no data')
      .classed('font-hint', true)
      .attr('x', canvasSize.width / 2)
      .attr('y', canvasSize.height / 2)
  },

  drawGraph: function (canvas, canvasSize, entries) {
    let scaler = this.getScaler(canvasSize, entries)

    let xAxis = d3.axisBottom().scale(scaler.xScale)
    let yAxis = d3.axisLeft().scale(scaler.yScale)

    let datum = this.convertToDatum(entries, scaler)

    let lineGenerator = d3.line()
      .x(v => v.date)
      .y(v => v.value)

    canvas.append('g')
      .attr('transform', `translate(0, ${canvasSize.height})`)
      .call(xAxis)
    canvas.append('g')
      .call(yAxis)
    canvas.append('path')
      .datum(datum)
      .attr('class', 'line')
      .attr('d', lineGenerator)

    this.addText(canvas, datum)
  },

  getSVG: function () {
    let el = d3.select(this.root).select('svg')
    if (el.node() === null) {
      return d3.select(this.root).append('svg')
    } else {
      return el
    }
  },

  resizeSVG: function (svg, size) {
    return svg
      .attr('width', size.width)
      .attr('height', size.height)
  },

  clearSVG: function (svg) {
    svg.selectAll('*').remove()
  },

  buildCanvas: function (svg) {
    return svg.append('g')
      .attr('transform', `translate(${this.inset.x}, ${this.inset.y})`)
  },

  getCanvasSize: function (svgSize) {
    return {
      width: svgSize.width - 2 * this.inset.x,
      height: svgSize.height - 2 * this.inset.y
    }
  },

  getScaler: function (canvasSize, entries) {
    let x = d3.scaleTime().rangeRound([0, canvasSize.width])
    let y = d3.scaleLinear().rangeRound([canvasSize.height, 0])

    x.domain(d3.extent(entries, e => e.date))
    y.domain(d3.extent(entries, e => e.value))

    return {
      xScale: x,
      yScale: y
    }
  },

  convertToDatum: function (entries, scaler) {
    return entries.map(e => {
      return {
        date: scaler.xScale(e.date),
        value: scaler.yScale(e.value),
        text: e.getValue()
      }
    })
  },

  addText: function (canvas, datum) {
    for (let entry of datum) {
      canvas.append('text')
        .datum(entry)
        .text(entry.text)
        .attr('x', entry.date)
        .attr('y', entry.value)
    }
  }
}
