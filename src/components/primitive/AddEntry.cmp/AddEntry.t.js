export default {
  ru: {
    lblAdd: 'Добавить',
    lblInputPlaceholder: 'Введите значение'
  },
  en: {
    lblAdd: 'Add',
    lblInputPlaceholder: 'Type in value'
  }
}
