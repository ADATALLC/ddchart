export default {
  ru: {
    lblAddEntryTitle: 'Добавить значение',
    lblValuesList: 'Список значений'
  },
  en: {
    lblAddEntryTitle: 'Add chart entry',
    lblValuesList: 'List of entries'
  }
}
