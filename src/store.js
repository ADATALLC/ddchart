import Vue from 'vue'
import Vuex from 'vuex'

import ChartEntry from './classes/ChartEntry'
import { Save } from './plugins/vuex-storage'

import { mutation } from './enums/vuex-storage.mutations'

Vue.use(Vuex)

export const state = {
  entries: []
}

export const getters = {

  entry: (state) => (id, defaultValue = null) => {
    return (state.entries || []).find(i => i.id === id) || defaultValue
  },

  entries: (state) => {
    return state.entries || []
  }

}

export const mutations = {

  [mutation.M_ADD_ENTRY]: (state, entry) => {
    if (entry instanceof ChartEntry) {
      let index = state.entries.findIndex(e => e.id === entry.id)
      if (!~index) {
        state.entries.push(entry)
      }
    }
  },

  [mutation.M_INSERT_ENTRY]: (state, {index, entry}) => {
    if (entry instanceof ChartEntry) {
      let index = state.entries.findIndex(e => e.id === entry.id)
      if (!~index) {
        state.entries.splice(index, 0, entry)
      }
    }
  },

  [mutation.M_UPDATE_ENTRY]: (state, entry) => {
    if (entry instanceof ChartEntry) {
      let index = state.entries.findIndex(e => e.id === entry.id)
      if (~index) {
        state.entries.splice(index, 1, entry)
      }
    }
  },

  [mutation.M_REMOVE_ENTRY]: (state, id) => {
    let index = state.entries.findIndex(e => e.id === id)
    if (~index) {
      state.entries.splice(index, 1)
    }
  },

  [mutation.M_REMOVE_ALL]: (state) => {
    state.entries = []
  }

}

export const actions = {

  addEntry: ({commit, getters}, entry) => {
    commit(mutation.M_ADD_ENTRY, entry)
  },

  addEntries: ({commit, getters}, entries) => {
    for (let entry of entries) {
      commit(mutation.M_ADD_ENTRY, entry)
    }
  },

  insertEntry: ({commit, getters}, {index, entry}) => {
    commit(mutation.M_INSERT_ENTRY, {index, entry})
  },

  updateEntry: ({commit, getters}, entry) => {
    commit(mutation.M_UPDATE_ENTRY, entry)
  },

  removeEntry: ({commit}, id) => {
    commit(mutation.M_REMOVE_ENTRY, id)
  },

  removeAll: ({commit}) => {
    commit(mutation.M_REMOVE_ALL)
  }

}

export default new Vuex.Store({
  plugins: [Save],
  state,
  getters,
  mutations,
  actions
})
