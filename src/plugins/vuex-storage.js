import ChartEntry from './../classes/ChartEntry'

import { mutation } from './../enums/vuex-storage.mutations'

import localForage from 'localforage'
let storage = localForage.createInstance({
  name: 'DDChart',
  version: 1.0,
  size: 10 * 1024 * 1024,
  storeName: 'DDChart_Storage'
})

/**
 * Функция сохранения данных Vuex
 * Возвращает функцию-плагин, региструрующую любую активность
 * измения данных хранилища на основании списка мутаций 'vuex-storage.mutation'
 *
 * @param store Ссылка на объект хранилища
 */
export function Save (store) {
  store.subscribe(({type, payload}, state) => {
    switch (type) {
      case mutation.M_ADD_ENTRY:
      case mutation.M_INSERT_ENTRY:
      case mutation.M_UPDATE_ENTRY:
        storage.setItem(
          payload.id.toString(),
          JSON.stringify({
            date: payload.date,
            value: payload.value
          })
        )
        break
      case mutation.M_REMOVE_ENTRY:
        storage.removeItem(payload)
        break
      case mutation.M_REMOVE_ALL:
        storage.clear()
        break
      default:
        console.error('Unknown VUEX mutation type!')
    }
  })
}

/**
 * Возвращает сохраненные данные предыдущей сессии
 * либо пустой массив
 *
 * @returns [ChartEntry] || []
 */
export async function Load () {
  return new Promise((resolve, reject) => {
    let entries = []

    storage.iterate((value, key, i) => {
      entries.push(new ChartEntry(
        Object.assign({
          id: key
        }, JSON.parse(value))
      ))
    }).then(() => {
      entries.sort((a, b) => a.date - b.date)
      resolve(entries)
    }).catch(error => {
      console.log(error)
      resolve([])
    })
  })
}
