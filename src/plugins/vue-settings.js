export default
{
  install: function (Vue, options) {
    Object.defineProperty(Vue.prototype, '$settings', { value: new Settings() })
  }
}
/**
 * Быстро-плагин VUE для централизованного хранения настроек приложения
 *
 * @export
 * @class Settings
 */
export class Settings {
  // состояние левой боковой панели
  //
  get LeftPaneIsCollpased () {
    let value = localStorage.getItem('settings_LeftPaneIsCollpased')
    return value ? value === 'true' : true
  }
  set LeftPaneIsCollpased (value) {
    localStorage.setItem('settings_LeftPaneIsCollpased', value)
  }
  // // // // //

  // состояние правой боковой панели
  //
  get RightPaneIsCollpased () {
    let value = localStorage.getItem('settings_RightPaneIsCollpased')
    return value ? value === 'true' : true
  }
  set RightPaneIsCollpased (value) {
    localStorage.setItem('settings_RightPaneIsCollpased', value)
  }
  // // // // //
}
