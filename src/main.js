import Vue from 'vue'
import App from './App.vue'
import store from './store'
import MultiLanguage from 'vue-multilanguage'
import Settings from './plugins/vue-settings'
import { Load } from './plugins/vuex-storage'

import 'normalize.css'
import './css/common-styles.scss'
import './css/animations.css'

Vue.use(MultiLanguage, {default: 'ru', ru: {}, en: {}})
Vue.use(Settings)
Vue.config.productionTip = false

window.Vue = new Vue({
  store,
  render: h => h(App)
}).$mount('#app')

Load().then(entries => {
  store.dispatch('addEntries', entries)
})
